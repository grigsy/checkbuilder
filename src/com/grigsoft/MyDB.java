package com.grigsoft;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class MyDB {
    protected abstract String[] getDefaults();
    protected abstract String getName();    // for args detection, lowercase
    protected abstract void handleString(String s, String sSepar);    // for args detection, lowercase
    public void load(String args[])
    {
        FileLoader fl=new FileLoader();
        String sUnique=getName();
        for (String s:args)
        {
            String sLower = s.toLowerCase();
            if (!sLower.startsWith(sUnique))
                continue;
            if (fl.Load(sLower.substring(sUnique.length()+1)))
                break;
            System.out.println("Using demo "+ sUnique + " list: failed to read DB from "+sLower.substring(6));
            break;
        }
        if (fl.isEmpty())
            makeDefaults(fl);
        String sSep=fl.detectSeparator();
        for (String s: fl) {
            handleString(s, sSep);
        }
    }
    protected void makeDefaults(ArrayList<String> list) {
        list.addAll(Arrays.asList(getDefaults()));
    }
}
