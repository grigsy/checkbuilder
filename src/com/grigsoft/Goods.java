package com.grigsoft;
import java.util.ArrayList;
import java.util.HashMap;

class GoodsInfo
{
    public GoodsInfo(String code, String name, double price, int bulk)
    { sCode=code; sName=name; dPrice=price; nBulkDiscount=bulk;}
    public GoodsInfo(String code, String name, String price, String bulk)
    { sCode=code; sName=name; dPrice=Double.parseDouble(price);
        nBulkDiscount= Integer.parseInt(bulk);}
    public String sCode;
    public String sName;
    public double dPrice;
    public int   nBulkDiscount;    // minimal number to start discount or 0
}

public class Goods extends MyDB{
    @Override protected String[] getDefaults()
    {
        // can't find easier declaration, so use strings
        return new String[]
                {
                // code, price, bulk, name
                "1;2.22;0;M&Ms 100g",
                "2;3.22;0;M&Ms 200g",
                "3;4.22;0;M&Ms 400g",
                "10;1.00;5;Milk with some extra long name should be cut",
                "20;10;5;Soap",
                };
    }
    @Override protected String getName() {return "goods";}
    @Override protected  void handleString(String s, String sSepar)
    {
            String[] splits=s.split(sSepar, 4);
            if (splits.length!=4)
                return;
            m_Storage.put(splits[0],
                    new GoodsInfo(splits[0], splits[3], splits[1], splits[2]));
    }
    protected HashMap<String, GoodsInfo> m_Storage=new HashMap<>();
    public GoodsInfo FindGood(String sCode)
    {
        return m_Storage.get(sCode);
    }
}