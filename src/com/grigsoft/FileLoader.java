package com.grigsoft;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileLoader extends ArrayList<String> {
    public String detectSeparator()
    {
        if (isEmpty())
            return ";";
        int nComma =get(0).indexOf(',');
        int nSemiColon =get(0).indexOf(';');
        if (nComma>0 && (nComma<nSemiColon || nSemiColon<0) )
            return ",";
        return ";";
    }
    public boolean Load(String file) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            while (true) {
                String s = br.readLine();
                if (s == null)
                    break;
                add(s);
            }

            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            clear();    // delete loaded entries
            return false;
        }
        return !isEmpty();
    }
}
