package com.grigsoft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;


class Purchase
{
    public Purchase(GoodsInfo g, int c)
    {
        theGood = g;
        nCount = c;
    }
    public GoodsInfo theGood;
    public int nCount;
}

class Basket
{
    public ArrayList<Purchase> theList=new ArrayList<>();
    public DiscountCard offCard;
}

public class Main {
    protected static Goods m_Goods=new Goods();
    protected static Cards m_Cards = new Cards();
    public static Basket buildBasket(String[] args)
    {
        Basket B=new Basket();
        for (String sl: args)
        {
            String s = sl.toLowerCase();
            if (s.indexOf(":")>0)   // any special params, like goods:file
                continue;
            String[] splits=s.split("-");
            if (splits.length<2)
                continue;
            if (splits[0].equals("card")) {
                B.offCard = m_Cards.getDiscount(splits[1]);
                continue;
            }
            GoodsInfo theItem =m_Goods.FindGood(splits[0]);
            if (theItem!=null) {
                B.theList.add(new Purchase(theItem, Integer.parseInt(splits[1])));
            }
            else
                System.out.println("Галя! А как это пробивать? " + splits[0]);
        }
        return B;
    }

    public static void printCheck(Basket B)
    {
        double dSum=0;
        char[] chLine=new char[54];
        Arrays.fill(chLine, '=');
        String sHLine = new String(chLine);
        System.out.println(sHLine);
        String sDate = String.format("%tH:%<tM:%<tS %<td %<tB %<tY", new Date());
        System.out.printf("| %-10s %39s |%n", "GREEN", sDate);
        System.out.println(sHLine);
        for (Purchase p : B.theList) {
            double dPrice =p.nCount*p.theGood.dPrice;
            String sName;
            if (p.theGood.sName.length()<=20)
                sName = p.theGood.sName;
            else
                sName = p.theGood.sName.substring(0, 19);
            System.out.printf("| %-20s  $%-7.2f  x %-4d $%-10.2f |%n",
                    sName, p.theGood.dPrice, p.nCount, p.nCount*p.theGood.dPrice);
            if (p.theGood.nBulkDiscount>0 && p.theGood.nBulkDiscount<=p.nCount) {
                System.out.printf("|%37s  -$%-10.2f |%n", "Bulk discount", dPrice*0.1);
                dPrice *=0.9;   // -10% for bulks
            }
            dSum += dPrice;
        }
        if (B.offCard!=null && B.offCard.dDiscountPrc>0 && B.offCard.dDiscountPrc<100) {
            System.out.printf("|%52s|%n| %-37s $%-11.2f |%n", "", "Subtotal:", dSum);
            String sPrc = String.format("%2.1f%%", B.offCard.dDiscountPrc);
            System.out.printf("| Disc. card %s: -%-20s  -$%-10.2f |%n",
                    B.offCard.sCode, sPrc, dSum * B.offCard.dDiscountPrc / 100);
            dSum -= dSum * B.offCard.dDiscountPrc / 100;
        }

        System.out.printf("| %-37s $%-11.2f |%n", "Total:", dSum);
        System.out.println(sHLine);
        System.out.printf("| %-50s |%n", "kassier: igor green");
        System.out.println(sHLine);
    }
    public static void main(String[] args) {
        m_Goods.load(args);
        m_Cards.load(args);
        Basket theBasket = buildBasket(args);
        printCheck(theBasket);
    }
}
