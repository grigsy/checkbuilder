package com.grigsoft;
import java.util.HashMap;

class DiscountCard
{
    public DiscountCard(String s, String p)
    { sCode=s; dDiscountPrc=Double.parseDouble(p);}
    public String sCode;
    public double dDiscountPrc;
}

public class Cards extends MyDB{
    @Override protected String[] getDefaults()
    {
        return new String[] {
                //code-discount
                "11;10",
                "22;10",
                "33;10",
                "44;30", // vip!
        };
    }
    @Override protected String getName() {return "cards";}
    @Override protected  void handleString(String s, String sSepar)
    {
        String[] splits=s.split(sSepar);
        if (splits.length!=2)
            return;
        m_Storage.put(splits[0], new DiscountCard(splits[0], splits[1]));
    }
    public DiscountCard getDiscount(String ref)
    {
        return m_Storage.get(ref);
    }
    protected HashMap<String,DiscountCard> m_Storage=new HashMap<>();
}
